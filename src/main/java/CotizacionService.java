
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CotizacionService {

    private double valorPrecio;
    private Date valorFechaHora;

    public Cotizacion obtenerCotizacion(String json) throws IOException, ParseException, NumberFormatException {
        if (json == null || json.equals("")) {
            throw new IOException("La pagina que consulta no esta respondiendo correctamente");
        } else {
            valorFechaHora = null;
            String auxValorFechaHora = json.substring(20, 41);
            valorFechaHora = new SimpleDateFormat("MMM d, yyyy hh:mm:ss").parse(auxValorFechaHora);
            String auxValorPrecio = json.substring(374, 382);
            auxValorPrecio = auxValorPrecio.replace(",", "");
            valorPrecio = Double.parseDouble(auxValorPrecio);
        }
        return new Cotizacion(valorPrecio, valorFechaHora);
    }
}

