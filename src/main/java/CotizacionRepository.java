
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class CotizacionRepository {

    private String json;

    public String consultarBitCoin(String jsonWS) throws MalformedURLException, ProtocolException, IOException {
        URL obj = new URL(jsonWS);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        try (BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()))) {
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                json = response.append(inputLine).toString();
            }
            in.close();
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return json;
    }
}
