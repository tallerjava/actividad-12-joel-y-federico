
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Cotizacion {

    private double precio;
    private Date fechaHora;

    Cotizacion(double precio, Date fechaHora) {
        this.precio = precio;
        this.fechaHora = fechaHora;
    }

    public double getPrecio() {
        return precio;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    @Override
    public String toString() {
        SimpleDateFormat formateador = new SimpleDateFormat("d-M-yyyy hh:mm");
        return formateador.format(fechaHora) + " / $ " + this.precio;
    }
}
