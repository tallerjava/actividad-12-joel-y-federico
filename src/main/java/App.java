
import java.io.IOException;
import java.net.ProtocolException;
import java.text.ParseException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class App {

    public static void main(String[] args) throws Exception {
        CotizacionRepository cotyRepo = new CotizacionRepository();
        CotizacionService cotizacionService = new CotizacionService();
        Cotizacion cotizacion;
        try {
            cotizacion = cotizacionService.obtenerCotizacion(cotyRepo.consultarBitCoin("https://api.coindesk.com/v1/bpi/currentprice.json"));
            try (Scanner sc = new Scanner(System.in)) {
                System.out.println("Presionar ENTER para ver la cotización");
                String enterkey = sc.nextLine();
                while (enterkey.isEmpty()) {
                    System.out.println("==Fecha=============Precio==");
                    System.out.println(cotizacion.toString());
                    System.out.println("============================");
                    System.out.println("Presionar ENTER para ver una nueva cotización");
                    enterkey = sc.nextLine();
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
