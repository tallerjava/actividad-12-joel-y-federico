
import java.io.IOException;
import java.text.ParseException;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestCotizacionService {

    @Test(expected = IOException.class)
    public void obtenerCotizacion_servicioResponde_errorIOException() throws IOException, ParseException {
        CotizacionService cotyServ = new CotizacionService();
        cotyServ.obtenerCotizacion(null);
    }

    @Test(expected = ParseException.class)
    public void obtenerCotizacion_servicioResponde_errorParseException() throws ParseException, IOException {
        CotizacionService cotyServ = new CotizacionService();
        cotyServ.obtenerCotizacion("asdasdsdasdasdasdasJu 11, 2018 00:49:00 dasdasddasdasdasdddddddddddddddddddddddasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdddddddddddddddddddddddddddddddddddddddddasdasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdasdasdasdasddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd6,380.3713dddasdasdasdasdasd");
    }

    @Test(expected = NumberFormatException.class)
    public void obtenerCotizacion_servicioResponde_errorNumberFormatException() throws ParseException, IOException {
        CotizacionService cotyServ = new CotizacionService();
        cotyServ.obtenerCotizacion("asdasdsdasdasdasdas Jul 11, 2018 00:49:00 dasdasddasdasdasdddddddddddddddddddddddasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdddddddddddddddddddddddddddddddddddddddddasdasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdasdasdasdasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddasdasdasdasddddasdasdasdasdasd");
    }
}
