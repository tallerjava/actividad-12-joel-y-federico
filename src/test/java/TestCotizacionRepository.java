
import java.io.IOException;
import java.net.MalformedURLException;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestCotizacionRepository {

    @Test(expected = MalformedURLException.class)
    public void obtenerCotizacion_UrlInvalida_errorMalformedURLException() throws MalformedURLException, IOException {
        CotizacionRepository cotyServ = new CotizacionRepository();
        cotyServ.consultarBitCoin("asdasdsdasdasdasdasdasdasddasdasdasdddddddddddddddddddddddasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdddddddddddddddddddddddddddddddddddddddddasdasdaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaasdasdasdasdasdddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddasdasdasdasddddasdasdasdasdasd");
    }
}
